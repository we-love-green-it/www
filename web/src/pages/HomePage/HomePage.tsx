import { Container } from '@chakra-ui/react'

import { MetaTags } from '@redwoodjs/web'
import BlogPreview from 'src/components/BlogPreview'

import Footer from 'src/components/Footer'
import Header from 'src/components/Header'
import Hero from 'src/components/Hero'
import JoinOurTeam from 'src/components/JoinOurTeam'

const HomePage = () => {
  return (
    <>
      <MetaTags title="Home" description="Home page" />
      <Container maxW="full">
        <Header />
        <Hero />
        <BlogPreview />
        <JoinOurTeam />
        <Footer />
      </Container>
    </>
  )
}

export default HomePage
